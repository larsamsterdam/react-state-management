import React from "react";
import ReactDOM from "react-dom";

const HelloMessage = ({name}) => <div>Hello {name}</div>;

const app = document.getElementById("app");
ReactDOM.render(<HelloMessage name="Qualogy" />, app);