# Task 04

## React Context to the rescue

*To keep a clear structure for our solutions we now work from the **[../react-context](../react-context/)** directory.*

Now that we have build the app with only using state and props you can see problems emerging.
We have to pass down the state via props to each level. Making the application a lot more open to error and creates much more work.

How to avoid prop drilling and pass data in the background?

Lets make use or **[React Context](https://reactjs.org/docs/context.html)** to manage our state

> Context provides a way to pass data through the component tree without having to pass props down manually at every level.

Pass the state of app.js to context with a context 'provider' and consume the state on the components with a context 'consumer'.

Features:

- Make the app.js state available to sub components with createContext and a provider
- Use context Consumer to consume the provided state of app.js on the relevant component
    
### Run tests using yarn test | or npm run test


Advanced:
- [Consuming multiple Contexts](https://reactjs.org/docs/context.html#consuming-multiple-contexts) - make it possible to not only add accounts, but also add saving goals
- Use the new context hook to [replace the Context.Consumers](https://alligator.io/react/usecontext/#enter-usecontext).