# Task 07

## Implement State Management with MobX

*To keep a clear structure for our solutions we now work from the **[../react-mobx](../react-mobx/)** directory. You can copy all code from the previous task to this folder and reuse it here*

So you might have heard of another state management library besides Redux, named MobX.
MobX is a little easier to pick up. Since most developers are familiar with OOP, you can understand MobX faster. States in MobX are mutable, aka: can be overwritten. You can simply update a state with the new values. From a functional programming point of view this can be considered "impure".


[Source](https://codeburst.io/mobx-vs-redux-with-react-a-noobs-comparison-and-questions-382ba340be09)


Read [Redux Toolkit](https://redux-toolkit.js.org/tutorials/basic-tutorial) to get the general idea. 

Features:

- Remove reducer.js
- Create store.js and add a MobX observable
- Remove Provider, useSelector and other Redux tools from all components
- Wrap all components that need shared state in an mobx-react observer function
- Import the store on all components that need the state or/and need the actions from the store
- Replace setAccounts, increment and decrement in the components with the actions from the store

Bonus:
- Update unit-tests

Advanced:
- Check out MobX Developer Tools
