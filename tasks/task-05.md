# Task 05

## Enhance the context with useReducer

> useReducer is usually preferable to useState when you have complex state logic that involves multiple sub-values or when the next state depends on the previous one.

The useReducer hook is an alternative to useState. It's mostly use for the more complex state. useReducer accepts a reducer function with the initial state of our React app, and returns the current state, then dispatches a function.

This will be much more clear, when we start implementing it. Now, we've to create a new file reducer.js in our src folder.

Read [useReducer](https://reactjs.org/docs/hooks-reference.html#usereducer) if you need tips to integrate. 

Features:

- Create reducer.js
- Import reducer in app.js
- Bind useReducer to app state
- Pass state and dispatch to provider
- Use dispatch to setAccounts
    
### Run tests using yarn test | or npm run test

Advanced:
- Replace increment and decrement function from counter with reducer.js 
