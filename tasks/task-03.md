# Task 03

Change the state of app.js to not only hold the amount of accounts but also the value of the accounts.
Pass down the value of the accounts down to the counters. Also pass down the way to update the account balances to the counters.

Show counters with values, based on the amount of accounts and balances in the state of app.js

Features:

- Create a accounts page component in **react-parcel/src/components/AccountsPage.js.** and add it to app.js
- Create a accounts panel component in **react-parcel/src/components/AccountsPanel.js.** and add it to AccountsPage.js
- Create a bank stats component displaying to amount of accounts in **react-parcel/src/components/BankStats.js.** and add it to AccountsPage.js
- Move button to create accounts from app.js to separate component **react-parcel/src/components/AddAccount.js.** and add it to BankStats.js
- Pass down the account balances via props from app.js to the counters
- Pass down the amount of accounts via props from app.js to the BankStats
- Pass down the way to update the account balances from app.js to the counters
- Pass down the way to update add account balances from app.js to the counters
    
### Run tests using yarn test | or npm run test

Advanced:
- [Unstated](https://github.com/jamiebuilds/unstated) - pass state without prop drilling

Bonus:
- Add unit-tests for BankStats and AddAccount