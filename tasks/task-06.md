# Task 06

## Implement State Management with Redux

*To keep a clear structure for our solutions we now work from the **[../react-redux](../react-redux/)** directory. You can copy all code from the previous task to this folder and reuse it here*

Now that you have been introduced to the useReducer hook. It is a small step to implement the state management library Redux. You might have had a small introduction before and it can look quit daunting. But as tech is ever evolving, so are the options with Redux. In 2020 the official recommended way to use Redux is to use the Redux Toolkit.


Read [Redux Toolkit](https://redux-toolkit.js.org/tutorials/basic-tutorial) to get the general idea. 

Features:

- Remove MyContext.js
- Adjust reducer.js, use createAction and createReducer functions from the redux toolkit to create the new reducer (https://redux-toolkit.js.org/tutorials/basic-tutorial#introducing-createreducer)
- Create the store in reducer.js with configureStore from Redux Toolkit
- In app.js import the store
- In app.js remove MyContext and replace MyContext.provider with the Provider from react-redux and add the store
- Replace accounts and dispatch with useSelector from react-redux in BankStats, AddAccount, AccountsPanel and Counter
- Pass state to provider
- Use useSelector in BankStats, AccountsPanel, AddAccount, instead of the "old" connect
- Use store.dispatch and import actions from reducer.js to add accounts, increment and decrement


Bonus:
- Redux DevTools - show state history

Advanced:
- Research the Redux Ducks pattern (https://github.com/erikras/ducks-modular-redux) 
- Update unit-tests to support Redux
