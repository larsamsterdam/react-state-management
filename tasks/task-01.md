# Task 01

Implement a Counter in **react-parcel/src/components/Counter.js.**

Use component's state to store the value of the counter.

Features:

- \+ button increments the value
- \- button decrements the value

Advanced:
- Apply business rule that counter cant get under 0
- Make [functional component](https://reactjs.org/docs/hooks-intro.html) (use react hooks) and [class component](https://reactjs.org/docs/state-and-lifecycle.html#adding-local-state-to-a-class) version (use react state)