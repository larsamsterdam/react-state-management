# Task 02

Add state to app.js which holds the amount of counters (accounts).

Show counters, based on the amount of accounts in the state of app.js

Implement a 'Add account' button in **react-parcel/src/components/AddAccount.js.** which adds a new account to the app.js state

Features:

- button for opening new accounts
- account balances are independent
- counter component accepts value as prop

### Run tests using yarn test | or npm run test

Feel free to ask questions

Advanced:
- [The concept of immutability](https://itnext.io/why-concept-of-immutability-is-so-damn-important-for-a-beginner-front-end-developer-8da85b565c8e) and [Immer.JS](https://immerjs.github.io/immer/docs/introduction)