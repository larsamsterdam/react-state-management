# React: State Management - Workshop / Webinar

Managing state is arguably the hardest part of any application. It's why there are so many state management libraries available and more coming around every day. But, which one is the best for your needs?

We organize this webinar to give you a better understanding of what state management is and learn to implement it in #React. By doing code challenges you will be introduced to multiple solutions for managing state including: Context API, Redux and MobX.

To be able to follow you need some base knowledge of JavaScript.
If you need a refresh, be sure to watch these video's:

- [Learn ES6](https://www.youtube.com/watch?v=WZQc7RUAg18)
- [Get started with React in 10 minutes](https://youtu.be/K02AkMbV1HM)

You will need the starting files to follow along:
[GitLab Repo](https://gitlab.com/larsamsterdam/react-state-management)

## Workshop guidelines

- This workshop is broken into several steps. Follow the presentation and perform the tasks from the ./tasks folder (e.g. [task-01.md](./tasks/task-01.md)). 
- Total amount of tasks: 07
- With each task create a branch from this repo and name it: task-X (e.g. task-01) - as starting point for the step
- Work on task-X branch. Use npm or yarn to “start” and check
- Need some assistance? Branch task-X-solution (e.g. task-1-solution) contains an example solution
- To easy? Take a look at Advanced topics for each task and learn more.


